#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <shop>

#define PLUGIN_VERSION	"2.0.0"

#define CATEGORY	"nades"

#pragma newdecls required
Handle kv;
char sNadeMdl[MAXPLAYERS+1][PLATFORM_MAX_PATH];

public Plugin myinfo =
{
	name = "[Shop] Nades",
	author = "FrozDark & ShaRen(some edit)",
	description = "Additional plugin to shop - Nades",
	version = PLUGIN_VERSION,
	url = "http://www.hlmod.ru/"
};

public void OnPluginStart()
{
	RegAdminCmd("nades_reload", Command_Reload, ADMFLAG_ROOT, "Reloads nades configuration");
	if (Shop_IsStarted()) Shop_Started();
}

public Action Command_Reload(int client, int args)
{
	OnPluginEnd();
	if (Shop_IsStarted()) Shop_Started();
	OnMapStart();
	ReplyToCommand(client, "Nades configuration successfuly reloaded!");
	return Plugin_Handled;
}

public void OnPluginEnd()
{
	Shop_UnregisterMe();
}

public int Shop_Started()
{
	if (kv == INVALID_HANDLE)
		OnMapStart();
	char buffer[64]; char desc[64];
	KvGetString(kv, "name", buffer, sizeof(buffer));
	KvGetString(kv, "description", desc, sizeof(desc));
	CategoryId category_id = Shop_RegisterCategory(CATEGORY, buffer, desc);
	char item[64]; char model[PLATFORM_MAX_PATH];
	if (KvGotoFirstSubKey(kv)) {
		do {
			if (KvGetSectionName(kv, item, sizeof(item))) {
				KvGetString(kv, "model", model, sizeof(model));
				int pos = FindCharInString(model, '.', true);
				if (pos != -1 && StrEqual(model[pos+1], "mdl", false) && Shop_StartItem(category_id, item)) {
					KvGetString(kv, "name", buffer, sizeof(buffer), item);
					KvGetString(kv, "description", desc, sizeof(desc));
					int price = KvGetNum(kv, "price", 5000);
					Shop_SetInfo(buffer, desc, price, RoundToNearest(price*0.7), Item_Togglable, KvGetNum(kv, "duration", 86400));
					Shop_SetCallbacks(_, OnEquipItem);
					if (KvJumpToKey(kv, "attributes")) {
						Shop_KvCopySubKeysCustomInfo(kv);
						KvGoBack(kv);
					}
					Shop_SetCustomInfo("level", KvGetNum(kv, "level", 0));
					Shop_EndItem();
					PrecacheModel(model, true);
				}
			}
		} while (KvGotoNextKey(kv));
	}
	KvRewind(kv);
}

public void OnMapStart()
{
	char buffer[PLATFORM_MAX_PATH];
	if (kv != INVALID_HANDLE)
		CloseHandle(kv);
	kv = CreateKeyValues("Nades");
	
	Shop_GetCfgFile(buffer, sizeof(buffer), "nades.txt");
	
	if (!FileToKeyValues(kv, buffer))
		SetFailState("Couldn't parse file %s", buffer);
	KvRewind(kv);
	if (KvGotoFirstSubKey(kv)) {
		do {
			KvGetString(kv, "model", buffer, sizeof(buffer));
			int pos = FindCharInString(buffer, '.', true);
			if (pos != -1 && StrEqual(buffer[pos+1], "mdl", false))
				PrecacheModel(buffer, true);
		} while (KvGotoNextKey(kv));
	}
	KvRewind(kv);
	
	Shop_GetCfgFile(buffer, sizeof(buffer), "nades_downloads.txt");
	File_ReadDownloadList(buffer);
}

public ShopAction OnEquipItem(int client, CategoryId category_id, const char[] category, ItemId item_id, const char[]item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		sNadeMdl[client][0] = '\0';
		return Shop_UseOff;
	}
	Shop_ToggleClientCategoryOff(client, category_id);
	if (KvJumpToKey(kv, item, false)) {
		KvGetString(kv, "model", sNadeMdl[client], sizeof(sNadeMdl[]));
		KvRewind(kv);
		if (!sNadeMdl[client][0]) {
			PrintToChat(client, "Failed to use \"%s\"!.", item);
			return Shop_Raw;
		}
		return Shop_UseOn;
	}
	PrintToChat(client, "Failed to use \"%s\"!.", item);
	return Shop_Raw;
}

public void OnClientDisconnect_Post(int client)
{
	sNadeMdl[client][0] = '\0';
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if (!strcmp(classname, "hegrenade_projectile"))
		SDKHook(entity, SDKHook_SpawnPost, OnHeSpawned);
}

public void OnHeSpawned(int entity)
{
	if (GetEntProp(entity, Prop_Data, "m_nNextThinkTick") == -1)
		return;
	int client = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if (0<client<=MaxClients && sNadeMdl[client][0])
		SetEntityModel(entity, sNadeMdl[client]);
}



char _smlib_empty_twodimstring_array[][] = { { '\0' } };
stock void File_AddToDownloadsTable(const char[] path, bool recursive=true, const char[][] ignoreExts=_smlib_empty_twodimstring_array, int size=0)
{
	if (path[0] == '\0')
		return;

	if (FileExists(path)) {
		char fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));
		if (StrEqual(fileExtension, "bz2", false) || StrEqual(fileExtension, "ztmp", false))
			return;
		if (Array_FindString(ignoreExts, size, fileExtension) != -1)
			return;
		AddFileToDownloadsTable(path);
		if (StrEqual(fileExtension, "mdl", false))
			PrecacheModel(path, true);
	}
	
	else if (recursive && DirExists(path)) {
		char dirEntry[PLATFORM_MAX_PATH];
		Handle __dir = OpenDirectory(path);
		while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {
			if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
				continue;
			Format(dirEntry, sizeof(dirEntry), "%s/%s", path, dirEntry);
			File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
		}
		CloseHandle(__dir);
	}
	else if (FindCharInString(path, '*', true)) {
		char fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));

		if (StrEqual(fileExtension, "*")) {
			char dirName[PLATFORM_MAX_PATH];
			char fileName[PLATFORM_MAX_PATH];
			char dirEntry[PLATFORM_MAX_PATH];

			File_GetDirName(path, dirName, sizeof(dirName));
			File_GetFileName(path, fileName, sizeof(fileName));
			StrCat(fileName, sizeof(fileName), ".");

			Handle __dir = OpenDirectory(dirName);
			while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {
				if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
					continue;
				if (strncmp(dirEntry, fileName, strlen(fileName)) == 0) {
					Format(dirEntry, sizeof(dirEntry), "%s/%s", dirName, dirEntry);
					File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
				}
			}
			CloseHandle(__dir);
		}
	}
	return;
}

stock bool File_ReadDownloadList(const char[] path)
{
	Handle file = OpenFile(path, "r");
	
	if (file  == INVALID_HANDLE)
		return false;

	char buffer[PLATFORM_MAX_PATH];
	while (!IsEndOfFile(file)) {
		ReadFileLine(file, buffer, sizeof(buffer));
		
		int pos;
		pos = StrContains(buffer, "//");
		if (pos != -1)
			buffer[pos] = '\0';
		pos = StrContains(buffer, "#");
		if (pos != -1)
			buffer[pos] = '\0';
		pos = StrContains(buffer, ";");
		if (pos != -1)
			buffer[pos] = '\0';
		TrimString(buffer);
		if (buffer[0] == '\0')
			continue;
		File_AddToDownloadsTable(buffer);
	}
	CloseHandle(file);
	return true;
}

stock void File_GetExtension(const char[] path, char[] buffer, int size)
{
	int extpos = FindCharInString(path, '.', true);
	
	if (extpos == -1) {
		buffer[0] = '\0';
		return;
	}
	strcopy(buffer, size, path[++extpos]);
}

stock int Math_GetRandomInt(int min, int max)
{
	int random = GetURandomInt();
	if (!random)
		random++;
	return RoundToCeil(float(random) / (float(2147483647) / float(max - min + 1))) + min - 1;
}

stock int Array_FindString(const char[][] array, int size, const char[] str, bool caseSensitive=true, int start=0)
{
	if (start < 0)
		start = 0;

	for (int i=start; i < size; i++)
		if (StrEqual(array[i], str, caseSensitive))
			return i;
	return -1;
}

stock bool File_GetFileName(const char[] path, char[] buffer, int size)
{
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	File_GetBaseName(path, buffer, size);
	
	int pos_ext = FindCharInString(buffer, '.', true);

	if (pos_ext != -1)
		buffer[pos_ext] = '\0';
}

stock bool File_GetDirName(const char[] path, char[] buffer, int size)
{
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	int pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1) {
		pos_start = FindCharInString(path, '\\', true);
		
		if (pos_start == -1) {
			buffer[0] = '\0';
			return;
		}
	}
	strcopy(buffer, size, path);
	buffer[pos_start] = '\0';
}

stock bool File_GetBaseName(const char[] path, char[] buffer, int size)
{
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	int pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1)
		pos_start = FindCharInString(path, '\\', true);
	
	pos_start++;
	
	strcopy(buffer, size, path[pos_start]);
}